async function lookupCode() {
	const lookupFeedback = document.getElementById("lookup-feedback");
	try {
		lookupFeedback.innerText = "";
		code = document.getElementById("lookup").value.replaceAll(" ", "").replaceAll('"', "");
		const res = await fetch(`https://r18.dev/videos/vod/movies/detail/-/dvd_id=${code}/json`);
		const video = await res.json();
		const content_id = video.content_id;
		window.location = `https://r18.dev/videos/vod/movies/detail/-/id=${content_id}/`;
	}
	catch {
		lookupFeedback.innerText = "Video not found.";
	}
}

function uncensor(input) {
	if (input) {
		const uncensored_words = ["Abuse", "Abused", "Asleep", "Assault", "Assaulted", "Assaulting", "Beat Up", "Bled", "Bleeding", "Blood", "Brutality", "Child", "Children", "Coprophilia", "Cruel", "Cruelty", "Disgrace", "Disgraced", "Drink", "Drug", "Drugged", "Drugging", "Drugs", "Drunk", "Elementary School", "Enforced", "Force", "Forced", "Forceful", "Forcing", "Gang Bang", "Gangbang", "Humiliated", "Humiliating", "Humiliation", "Hypno", "Hypnosis", "Hypnotism", "Hypnotize", "Hypnotized", "Illegal", "Junior High", "Kid", "Kidnap", "Kids", "Kill", "Killer", "Killing", "Kindergarten", "Loli", "Lolicon", "Lolita", "Mind Control", "Molest", "Molestation", "Molested", "Molester", "Molesting", "Mother And Son", "Nursery", "Passed Out", "Passing Out", "Poop", "Preschool", "Punish", "Punished", "Punisher", "Punishing", "Rape", "Raped", "Rapes", "Raping", "Rapist", "Rapists", "Scat", "Scatology", "School Girl", "School Girls", "Schoolgirl", "Schoolgirls", "Shota", "Shotacon", "Slave", "Sleeping", "Student", "Students", "Submission", "Tentacle", "Tentacles", "Torture", "Tortured", "Unconscious", "Unwilling", "Violate", "Violated", "Violation", "Violence", "Violent", "Young Girl"];
		const dictionary = {};
		const duplicates = [];
		uncensored_words.forEach(uncensored_word => {
			const censored_word = uncensored_word[0] + "\\*".repeat(uncensored_word.length - 2) + uncensored_word.slice(-1);
			if (dictionary[censored_word]) {
				duplicates.push(censored_word);
			}
			else {
				dictionary[censored_word] = uncensored_word;
			}
		});
		duplicates.forEach(duplicate => {
			delete dictionary[duplicate];
		});
		for (const censored_word in dictionary) {
			const uncensored_word = dictionary[censored_word];
			input = input.replaceAll(RegExp(censored_word, "gi"), match => match[0] + uncensored_word.slice(1, -1) + match.slice(-1));
		}
	}
	return input;
}
